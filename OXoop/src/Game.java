import java.util.Scanner;
public class Game {

	private Table table;
	private int row;
	private int col;
	private Player x;
	private Player o;
	Scanner kb = new Scanner(System.in);
	
	public Game() {
		x = new Player('X');
		o = new Player('O');
	}
	
	public void showWelcome() {
		System.out.println("Welcome to OX Game!!");
	}
	
	public void startGame() {
		table = new Table(o,x);
	}
	
	public void showTable() {
		char data[][] = table.getData();
		System.out.println(" 1 2 3");
		for(int i=0;i<3;i++) {
			System.out.print((i+1));
			for(int j=0;j<3;j++) {
				System.out.print(" "+data[i][j]);
			}
			System.out.println();
		}
	}
	
	public void showTurn() {
		System.out.println("It's "+table.getCurrentPlayer().getName() + " turn");
	}
	
	 public void showWin(Player player) {
	        this.showTable();
	        if (table.getWinner() == null) {
	            System.out.println("Draw");
	        } else {
	            System.out.println("Player " + player.getName() + " Win");
	        }
	        System.out.println("O: Win: " + o.getWin() + " Lose: " + o.getLose() + " Draw: " + o.getDraw());
	        System.out.println("X: Win: " + x.getWin() + " Lose: " + x.getLose() + " Draw: " + x.getDraw());
	}
	 
	public void showWin() {
        Player player = table.getWinner();
        this.showWin(player);
    }
	
    private static void showBye() {
        System.out.println("Bye bye.");
    }
    
    public boolean inputRowCol() {
        System.out.print("Please input Row Col: ");
        try {
            row = kb.nextInt();
            col = kb.nextInt();
            table.setRowCol(row, col);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean inputContinue() {
        System.out.print("Continue? (y/n): ");
        String c = kb.next();
        if (c.equals("y")) {
            return true;
        }
        return false;
    }
    
    public void run() {
        showWelcome();
        do {
            startGame();
            runOne();
        } while (inputContinue());
        showBye();
    }

    public void runOne() {
        while (true) {
            this.showTable();
            showTurn();
            if (this.inputRowCol()) {
                if (table.checkWin()) {
                    showWin();
                    return;
                } else if (table.checkDraw()) {
                    showWin();
                    return;
                }
            }
        }
    }
}
