import java.util.Random;

public class Table {
	
	private Player o;
	private Player x;
	private Player currentPlayer;
	private Player win;
	private char data[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
	private int turn ;
	Random random = new Random();
	
	public Table(Player o,Player x) {
		this.o = o;
		this.x = x;
		if (random.nextInt(2) + 1 == 1) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
        win = null;
	}
	
	public char[][] getData(){
		return data;
	}
	
	public Player getWinner() {
		return win;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public int getTurn() {
		return turn;
	}
	
	public void setWin() {
		if(win == x) {
			x.Win();
			o.Lose();
		}else if(win == o) {
			o.Win();
			x.Lose();
		}
	}
	
	public void switchTurn() {
		if(currentPlayer == o) {
    		currentPlayer = x;
    	}else {
    		currentPlayer = o;
    	}
	}
	
	public void setRowCol(int row,int col) {
		if(data[row - 1][col - 1] == ('-')) {
            data[row - 1][col - 1] = currentPlayer.getName();
            turn++;
        } else {
            data[99][99] = currentPlayer.getName();
        }
	}
	
	public boolean checkRow(int row) {
		for(int i=0;i<3;i++) {
			if(data[row][i] != currentPlayer.getName()) {
				return false;
			}
		}
		win = currentPlayer;
		setWin();
		return true;
	}
	
	public boolean checkCol(int col) {
		for(int i=0;i<3;i++) {
			if(data[i][col] != currentPlayer.getName()) {
				return false;
			}
		}
		win = currentPlayer;
		setWin();
		return true;
	}
	
	public boolean checkX1() {
		for(int i=0;i<3;i++) {
			if(data[i][i] != currentPlayer.getName()) {
				return false;
			}
		}
		win = currentPlayer;
		setWin();
		return true;
	}
	
	public boolean checkX2() {
		for(int i=0;i<3;i++) {
			if(data[2-i][0+i] != currentPlayer.getName()) {
				return false;
			}
		}
		win = currentPlayer;
		setWin();
		return true;
	}
	
	public boolean checkDraw() {
		if(turn == 9) {
			win = null;
			x.Draw();
			o.Draw();
			return true;
		}
		return false;
	}
	
	public boolean checkWin() {
		for(int i=0 ; i<3 ; i++) {
			if(checkRow(i)) {
				return true;
			}else if(checkCol(i)) {
				return true;
			}else if(checkX1()) {
				return true;
			}else if(checkX2()) {
				return true;
			}
		}
		switchTurn();
		return false;
	}
}
